import unittest

import requests

from example.my_lib import requests_version


class TestRequestsVersion(unittest.TestCase):
    def test_version(self):
        self.assertEqual(requests.__version__, requests_version())


if __name__ == '__main__':
    unittest.main()
