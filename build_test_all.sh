#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")"

set -e

find . -type f -name WORKSPACE | while read workspace ; do
  pushd $(dirname ${workspace})
    workspace="$(basename $(pwd))"

    bazel build //...
    if [ "${workspace}" = "cpp" ] || \
        [ "${workspace}" = "golang-simple" ] || \
        [ "${workspace}" = "lint" ] || \
        [ "${workspace}" = "python" ] || \
        [ "${workspace}" = "python-with-interpreter" ] || \
        [ "${workspace}" = "python-with-pip-cache" ]; then
      bazel test //...
    fi
  popd
done

echo "================================ ALL PASS ==============================="
