#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")"

TARGET_PYTHON_VERSION="3.10"

docker run -v $(pwd):/work -w /work python:${TARGET_PYTHON_VERSION} bash -c -- "\
    python3 -m pip install -r requirements.txt && \
    python3 -m pip freeze -r requirements.txt > requirements_lock.txt"
