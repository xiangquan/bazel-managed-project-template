def go_root(gocache="/tmp/gocache"):
    # go build ./...
    native.genrule(
        name = "go-build-rule",
        srcs = ["go.sum"],
        outs = ["go-build"],
        cmd = '''
            GO="$$(realpath $(location @go_sdk//:bin/go))"
            SRC_ROOT="$$(dirname $$(realpath $(location go.sum)))"
            touch $@

            cd $${SRC_ROOT}
            GOCACHE=%s $${GO} build ./...
        ''' % gocache,
        exec_tools = ["@go_sdk//:bin/go"],
    )

    # go test ./...
    native.genrule(
        name = "go-test-rule",
        srcs = ["go.sum"],
        outs = ["go-test.sh"],
        cmd = '''
            GO=$$(realpath $(location @go_sdk//:bin/go))
            SRC_ROOT="$$(dirname $$(realpath $(location go.sum)))"

            echo "
                cd $${SRC_ROOT}
                GOCACHE=%s $${GO} test ./...
            " > $@
        ''' % gocache,
        exec_tools = ["@go_sdk//:bin/go"],
    )

    native.sh_test(
        name = "go-test",
        srcs = ["go-test.sh"],
    )

def simple_go_binary(name, src, gocache="/tmp/gocache"):
    """Build a single binary."""
    native.genrule(
        name = name + "-rule",
        outs = [name],
        srcs = [
            src,
            "//:go.sum",
        ],
        executable = True,
        cmd = '''
            GO=$$(realpath $(location @go_sdk//:bin/go))
            GOCACHE=%s $${GO} build -o $@ "$(location %s)"
        ''' % (gocache, src),
        exec_tools = ["@go_sdk//:bin/go"],
    )
