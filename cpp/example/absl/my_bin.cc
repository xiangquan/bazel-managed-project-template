#include <iostream>

#include "absl/status/status.h"

absl::Status func() {
  return absl::OkStatus();
}

int main() {
  std::cout << (func().ok() ? "OK" : "Not OK") << std::endl;
  return 0;
}
