# Lint

Requirements:

```bash
conda install -c conda-forge autopep8 clang-format flake8
# Or
sudo apt install -y clang-format flake8 python3-autopep8
```
