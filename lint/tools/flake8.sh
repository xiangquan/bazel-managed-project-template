#!/usr/bin/env bash

if [ -z "$(which flake8)" ]; then
  echo "flake8 required."
  exit 1
fi

flake8 $@
