import glob
import os


# Required: go install github.com/bazelbuild/buildtools/buildifier@latest
BUILDIFIER_TARGETS = [
    'WORKSPACE',
    'BUILD',
    'tools/**/BUILD',
    'example/**/BUILD',
]

PY_TARGETS = [
    'tools/**/*.py',
    'example/**/*.py',
]

CLANG_TARGETS = [
    'example/**/*.cc',
    'example/**/*.h',
]


def main():
    # Go to project root.
    os.chdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
    for target in BUILDIFIER_TARGETS:
        for f in glob.glob(target, recursive=True):
            cmd = F'buildifier {f}'
            print(cmd)
            os.system(cmd)

    for target in PY_TARGETS:
        for f in glob.glob(target, recursive=True):
            cmd = F'autopep8 --max-line-length=100 -i {f}'
            print(cmd)
            os.system(cmd)

    for target in CLANG_TARGETS:
        for f in glob.glob(target, recursive=True):
            cmd = F'clang-format -i {f}'
            print(cmd)
            os.system(cmd)


if __name__ == '__main__':
    main()
