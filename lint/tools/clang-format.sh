#!/usr/bin/env bash

if [ -z "$(which clang-format)" ]; then
  echo "clang-format required."
  exit 1
fi

clang-format --dry-run --Werror $@
