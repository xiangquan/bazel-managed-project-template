CLANG_EXTENSIONS = [
    ".c",
    ".cc",
    ".cpp",
    ".h",
    ".hpp",
    ".java",
    ".js",
    ".json",
    ".proto",
]

PY_EXTENSIONS = [
    ".py",
]

def _get_labels_and_files_by_extensions(labels, extensions):
    result_labels = []
    result_files = []
    for label in labels:
        for ext in extensions:
            if label.endswith(ext):
                result_labels.append(label)
                result_files.append("$(location %s)" % label)
    return (result_labels, result_files)

def _add_cpp_rules(labels, name):
    cpp_labels, files = _get_labels_and_files_by_extensions(labels, CLANG_EXTENSIONS)
    if len(files) == 0:
        return
    native.sh_test(
        name = name + "_clang_format",
        srcs = ["//tools:clang-format.sh"],
        data = cpp_labels + ["//:.clang-format"],
        args = files,
        size = "small",
        tags = ["lint"],
    )

def _add_py_rules(labels, name):
    py_labels, files = _get_labels_and_files_by_extensions(labels, PY_EXTENSIONS)
    if len(files) == 0:
        return
    native.sh_test(
        name = name + "_flake8",
        srcs = ["//tools:flake8.sh"],
        data = py_labels + ["//:tox.ini"],
        args = files,
        size = "small",
        tags = ["lint"],
    )

def lint():
    for rule in native.existing_rules().values():
        # Extract the list of C++ source code labels and convert to filenames.
        labels = list(rule.get("srcs", ())) + list(rule.get("hdrs", ()))
        _add_cpp_rules(labels, rule["name"])
        _add_py_rules(labels, rule["name"])
