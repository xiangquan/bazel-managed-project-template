# Docker Image

```bash
# Simply generate rules.
bazel build //example

# Docker build, push and run fingerprint command.
bazel run //example -- build
bazel run //example -- push
bazel run //example -- fingerprint
```
