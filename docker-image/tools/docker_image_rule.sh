#!/usr/bin/env bash

set +e

if [ -z "`which docker`" ]; then
    echo "Docker not available, skip building docker image."
    exit 0
fi

source "PLACEHOLDER_UTILS_SH"

function docker_build {
    PLACEHOLDER_AUTH_CHECK
    export DOCKER_BUILDKIT=1
    export IN_CHINA=$(is_in_china)

    if [ "PLACEHOLDER_SRC_DIR" != "PLACEHOLDER_CONTEXT_DIR" ]; then
        cp -fr PLACEHOLDER_SRC_DIR/* PLACEHOLDER_CONTEXT_DIR/
    fi

    set -e
    cd "PLACEHOLDER_CONTEXT_DIR"
    cmd="docker build --platform linux/amd64 --build-arg IN_CHINA=${IN_CHINA} \
         PLACEHOLDER_BUILD_ARGS \
         -t PLACEHOLDER_IMAGE \
         -f PLACEHOLDER_DOCKERFILE"
    if ${IN_CHINA}; then
        ${cmd} --build-arg GHPROXY=https://ghproxy.com/ \
               --build-arg GOPROXY=https://goproxy.cn,direct \
               --build-arg GOSUMDB=sum.golang.google.cn \
               --build-arg NPM_REGISTRY=https://registry.npm.taobao.org/ \
               --build-arg PIP_OPTIONS="-i https://mirrors.aliyun.com/pypi/simple \
                                        --extra-index-url https://pypi.org/simple \
                                        --timeout 120" \
               .
    else
        ${cmd} .
    fi

    if [ ! -z "PLACEHOLDER_ALIAS_IMAGE" ]; then
        # Can be broken if it is not reachable.
        docker tag PLACEHOLDER_IMAGE PLACEHOLDER_ALIAS_IMAGE
    fi
}

function docker_push {
    if PLACEHOLDER_SKIP_EXISTING; then
        cache="/tmp/bazel/known-images.txt"
        touch "${cache}"
        if grep "^PLACEHOLDER_IMAGE$" "${cache}" ; then
            echo "Skip pushing known existing image PLACEHOLDER_IMAGE"
            exit 0
        elif docker pull PLACEHOLDER_IMAGE > /dev/null 2>&1 ; then
            echo "Skip pushing existing image PLACEHOLDER_IMAGE"
            echo "PLACEHOLDER_IMAGE" >> "${cache}"
            exit 0
        fi
    fi

    docker_build

    set -e
    docker push PLACEHOLDER_IMAGE
    if [ ! -z "PLACEHOLDER_ALIAS_IMAGE" ]; then
        # Can be broken if it is not reachable.
        docker push PLACEHOLDER_ALIAS_IMAGE
    fi
}

function docker_fingerprint {
    cd "PLACEHOLDER_SRC_DIR"
    if [ ! -z "PLACEHOLDER_FINGERPRINT_CMD" ] && [ ! -z "PLACEHOLDER_FINGERPRINT_TO" ]; then
        # Can be broken if it is not reachable.
        docker run --platform linux/amd64 --rm --entrypoint sh PLACEHOLDER_IMAGE -c -- \
            "PLACEHOLDER_FINGERPRINT_CMD" > "PLACEHOLDER_FINGERPRINT_TO"
    fi
}

if [ "$1" = "push" ]; then
    docker_push
elif [ "$1" = "fingerprint" ]; then
    docker_fingerprint
else
    docker_build
fi
