"""
Define a docker image, e.g.:

docker_image(
    name = "my-image",
    repo = "xxx/yyy",
    tag = "0.1",
)

1. Simply create the executable script
   - bazel build :my-image
2. Build the image
   - bazel run :my-image -- build
3. Push the image
   - bazel run :my-image -- push
4. Generate fingerprint of the image
   - bazel run :my-image -- fingerprint
"""

def docker_image(
        name,
        repo,
        tag,
        auth_required_to_build = [],
        build_args = [],
        data = [],
        dockerfile = "Dockerfile",
        fingerprint_cmd = "",
        fingerprint_to = "",
        skip_existing = True,
        tag_alias = ""):
    auth_check = ""
    for auth in auth_required_to_build:
        auth_check += "if ! grep %s ~/.docker/config.json; then exit 0; fi;" % auth
    build_args = " ".join([("--build-arg " + arg) for arg in build_args])
    alias_image = "{}:{}".format(repo, tag_alias) if tag_alias else ""

    # There is no generated data to be consumed, use source code dir as context directly to avoid
    # copying files.
    use_src_dir_as_context = (len(data) == 0)
    native.genrule(
        name = name + "-rule",
        outs = [name + ".sh"],
        srcs = [
            dockerfile,
            "//tools:docker_image_rule.sh",
            "//tools:utils.sh",
        ],
        cmd = """
            dockerfile_path="$$(realpath "$(location {dockerfile})")"
            utils_sh_path="$$(realpath "$(location //tools:utils.sh)")"
            src_dir="$${{dockerfile_path%/{dockerfile}}}"
            out_dir="$$(dirname "$$(realpath $@)")"
            if {use_src_dir_as_context}; then
                context_dir=$${{src_dir}}
            else
                context_dir=$${{out_dir}}
            fi

            cp -f "$(location //tools:docker_image_rule.sh)" $@
            sed -i "s|PLACEHOLDER_ALIAS_IMAGE|{alias_image}|g" $@
            sed -i "s|PLACEHOLDER_AUTH_CHECK|{auth_check}|g" $@
            sed -i "s|PLACEHOLDER_BUILD_ARGS|{build_args}|g" $@
            sed -i "s|PLACEHOLDER_CONTEXT_DIR|$${{context_dir}}|g" $@
            sed -i "s|PLACEHOLDER_DOCKERFILE|{dockerfile}|g" $@
            sed -i "s|PLACEHOLDER_FINGERPRINT_CMD|{fingerprint_cmd}|g" $@
            sed -i "s|PLACEHOLDER_FINGERPRINT_TO|{fingerprint_to}|g" $@
            sed -i "s|PLACEHOLDER_IMAGE|{image}|g" $@
            sed -i "s|PLACEHOLDER_SKIP_EXISTING|{skip_existing}|g" $@
            sed -i "s|PLACEHOLDER_SRC_DIR|$${{src_dir}}|g" $@
            sed -i "s|PLACEHOLDER_UTILS_SH|$${{utils_sh_path}}|g" $@

            # Make sure all placeholders are fulfilled.
            if grep "PLACEHOLDER" $@ ; then
                echo "Unsettled placeholders in docker-image rule."
                exit 1
            fi
        """.format(
            alias_image = alias_image,
            auth_check = auth_check,
            build_args = build_args,
            dockerfile = dockerfile,
            fingerprint_cmd = fingerprint_cmd,
            fingerprint_to = fingerprint_to,
            image = "{}:{}".format(repo, tag),
            skip_existing = str(skip_existing).lower(),
            use_src_dir_as_context = str(use_src_dir_as_context).lower(),
        ),
    )

    native.sh_binary(
        name = name,
        srcs = [name + ".sh"],
        tags = ["docker-image"],
        data = data,
    )
