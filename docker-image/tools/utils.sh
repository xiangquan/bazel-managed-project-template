function is_in_china() {
    # Usage:
    # export IN_CHINA=$(is_in_china)
    # if ${IN_CHINA}; then ...
    if [ ! -z "${IN_CHINA:-}" ]; then
        echo "${IN_CHINA}"
    else
        country=$(curl -s ipinfo.io | grep country | awk -F'"' '{print $4}')
        if [ "${country}" = "CN" ]; then
            echo "true"
        else
            echo "false"
        fi
    fi
}
