load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def proto_dependencies():
    # 2022/06/27
    http_archive(
        name = "bazel_gazelle",
        url = "https://github.com/bazelbuild/bazel-gazelle/releases/download/v0.26.0/bazel-gazelle-v0.26.0.tar.gz",
    )

    # 2022/04/14
    http_archive(
        name = "com_github_bazelbuild_buildtools",
        strip_prefix = "buildtools-5.1.0",
        url = "https://github.com/bazelbuild/buildtools/archive/5.1.0.tar.gz",
    )

    # 2022/07/11
    http_archive(
        name = "com_github_googleapis_googleapis",
        strip_prefix = "googleapis-220b13e335ea8e0153c84c56a135a19d15384621",
        url = "https://github.com/googleapis/googleapis/archive/220b13e335ea8e0153c84c56a135a19d15384621.zip",
    )

    # 2022/06/06
    http_archive(
        name = "io_bazel_rules_go",
        url = "https://github.com/bazelbuild/rules_go/releases/download/v0.33.0/rules_go-v0.33.0.zip",
    )

    # 2022/08/25
    http_archive(
        name = "rules_python",
        sha256 = "c03246c11efd49266e8e41e12931090b613e12a59e6f55ba2efd29a7cb8b4258",
        strip_prefix = "rules_python-0.11.0",
        url = "https://github.com/bazelbuild/rules_python/archive/refs/tags/0.11.0.tar.gz",
    )
