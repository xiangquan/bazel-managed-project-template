load("@bazel_gazelle//:deps.bzl", "gazelle_dependencies")
load("@build_bazel_rules_apple//apple:repositories.bzl", "apple_rules_dependencies")
load("@com_github_googleapis_googleapis//:repository_rules.bzl", "switched_rules_by_language")
load("@io_bazel_rules_go//go:deps.bzl", "go_register_toolchains", "go_rules_dependencies")

def proto_toolchains():
    apple_rules_dependencies()
    go_rules_dependencies()
    go_register_toolchains(version = "1.18.3")
    gazelle_dependencies()

    switched_rules_by_language(
        name = "com_google_googleapis_imports",
        cc = True,
        go = True,
        grpc = True,
        python = True,
    )
